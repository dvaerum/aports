# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-dask
pkgver=2.18.0
pkgrel=0
pkgdesc="Parallel computing with task scheduling"
url="https://dask.org/"
arch="noarch !mips !mips64 !s390x" # Blocked by py3-partd
license="BSD-3-Clause"
depends="python3 py3-toolz py3-numpy py3-pandas py3-fsspec py3-partd py3-yaml"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-runner"
source="https://pypi.python.org/packages/source/d/dask/dask-$pkgver.tar.gz"
builddir="$srcdir/dask-$pkgver"

case "$CARCH" in
	# Python segfaults while running the tests
	ppc64le) options="$options !check" ;;
esac

build() {
	python3 setup.py build
}

check() {
	# test_parquet.py requires not available packages
	pytest --ignore=dask/dataframe/io/tests/test_parquet.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="0f771cab7fa9809f86714e276ba6d9ecda029cfd7830f13b80f1059e9ad08a78a2200b2fd31b2d54043e13b16295ec59a7133582c50e7bcb1544ee5fce3dff37  dask-2.18.0.tar.gz"
